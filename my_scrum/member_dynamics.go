package my_scrum

import (
	"vtb_jira/dir_functions"
)

func GetDynamics(member Member, dir string) []int {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)

	var res []int

	for i := 0; i < len(allCsvFiles); i++ {

		res = append(res, GetPlaceInTheFile(member, allCsvFiles, allCsvFiles[i], dir))

	}

	return res
}

func GetPlaceInTheFile(member Member, allCsvFiles []string, file string, dir string) int {

	team := GetSprintTeam(allCsvFiles, dir)

	thisFileTeamRating := GetCurrentTeamRating(team, file, dir)

	for i := 0; i < len(thisFileTeamRating); i++ {
		if member.Name == thisFileTeamRating[i].Name {

			return i + 1
		}
	}

	return 0
}
