package my_scrum

import (
	"fmt"
	"vtb_jira/dir_functions"
	"vtb_jira/my_conv"
	"vtb_jira/my_csv"
)

type BurnDownPoint struct {
	Label          string
	SpBurnt        float64
	SpPerfectBurnt float64
	SpAll          float64
	SpTendency     float64
}

type BurnDownChart struct {
	Chart []BurnDownPoint
}

func PrintBurnDownChart(dir string) {

	var burnDownChart []BurnDownPoint
	burnDownChart = GenerateBurnDownData(dir)
	//fmt.Println(burnDownChart)

	fmt.Printf("{\n")
	fmt.Printf("label: 'Реальное сгорание',\n")
	fmt.Printf("backgroundColor: 'rgba(96,255,87,0.4)',\n")
	fmt.Printf("borderColor: 'rgb(96,255,87)',\n")
	fmt.Printf("pointRadius: 6,\n")
	fmt.Printf("data: [")

	for i := 0; i < len(burnDownChart); i++ {

		if i != len(burnDownChart)-1 {

			if burnDownChart[i].SpBurnt != -1 {
				fmt.Printf("%v,", burnDownChart[i].SpBurnt)
			}
		}

	}

	fmt.Printf("]\n},\n")

	fmt.Printf("{\n")
	fmt.Printf("label: 'Идеальное сгорание',\n")
	fmt.Printf("backgroundColor: 'rgba(76,86,191,0.1)',\n")
	fmt.Printf("borderColor: 'rgb(76,86,191)',\n")
	fmt.Printf("pointRadius: 0,\n")
	fmt.Printf("data: [")

	for i := 0; i < len(burnDownChart); i++ {
		if i != len(burnDownChart)-1 {
			fmt.Printf("%v,", burnDownChart[i].SpPerfectBurnt)
		} else {
			fmt.Printf("%v", burnDownChart[i].SpPerfectBurnt)
		}
	}

	fmt.Printf("]\n},\n")

	fmt.Printf("{\n")
	fmt.Printf("label: 'Тенденция',\n")
	fmt.Printf("backgroundColor: 'rgba(0, 0, 0, 0.0)',\n")
	fmt.Printf("borderColor: 'rgb(195,33,178)',\n")
	fmt.Printf("borderDash: [5],\n")
	fmt.Printf("pointRadius: 0,\n")
	fmt.Printf("data: [")

	for i := 0; i < len(burnDownChart); i++ {
		if i != len(burnDownChart)-1 {
			fmt.Printf("%v,", burnDownChart[i].SpTendency)
		} else {
			fmt.Printf("%v", burnDownChart[i].SpTendency)
		}
	}

	fmt.Printf("]\n},\n")

	fmt.Printf("{\n")
	fmt.Printf("label: 'Объем спринта',\n")
	fmt.Printf("backgroundColor: 'rgba(0, 0, 0, 0.0)',\n")
	fmt.Printf("borderColor: 'rgb(119,119,119)',\n")
	fmt.Printf("borderDash: [5],\n")
	fmt.Printf("pointRadius: 0,\n")
	fmt.Printf("data: [")

	for i := 0; i < len(burnDownChart); i++ {
		if i != len(burnDownChart)-1 {
			fmt.Printf("%v,", burnDownChart[i].SpAll)
		} else {
			fmt.Printf("%v", burnDownChart[i].SpAll)
		}
	}

	fmt.Printf("]\n},\n")

}

func GenerateBurnDownData(dir string) []BurnDownPoint {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)
	//fmt.Println(allCsvFiles)

	var burnDownChart []BurnDownPoint

	var i int
	for i = 0; i < len(allCsvFiles); i++ {

		burnDownChart = append(burnDownChart, getBurnDownPoint(allCsvFiles[i], dir, i, len(allCsvFiles)-1))
	}

	allSp := burnDownChart[i-1].SpAll

	if i < 12 {
		for i := i; i < 12; i++ {
			// здесь дозаполняем оставшиеся точки
			burnDownChart = append(burnDownChart, getEmptyBurnDownPoint(i, allSp))
		}
	}

	globalSpBurnt := burnDownChart[len(allCsvFiles)-1].SpAll - burnDownChart[len(allCsvFiles)-1].SpBurnt

	lastPointTendency := (globalSpBurnt * 10) / float64(len(allCsvFiles)-1)

	//fmt.Println("lastPointTendency", lastPointTendency)

	for i = 0; i < 11; i++ {

		burnDownChart[i].SpTendency = burnDownChart[i].SpAll - ((lastPointTendency * float64(i)) / 10)

	}

	return burnDownChart

}

func getBurnDownPoint(fileName string, dir string, index, countDaysForNow int) BurnDownPoint {

	parseData := my_csv.CsvParse(fileName, dir)
	//fmt.Println(parseData)

	var point BurnDownPoint

	point.SpAll = getSpAll(parseData)
	point.SpBurnt = getSpBurnt(parseData, point.SpAll)
	point.SpPerfectBurnt = getSpPerfectBurnt(point.SpAll, index)

	switch index {
	case 0:
		point.Label = "Пн(10:00 Мск.)"
	case 1:
		point.Label = "Вт(10:00 Мск.)"
	case 2:
		point.Label = "Ср(10:00 Мск.)"
	case 3:
		point.Label = "Чт(10:00 Мск.)"
	case 4:
		point.Label = "Пт(10:00 Мск.)"
	case 5:
		point.Label = "Пн(10:00 Мск.)"
	case 6:
		point.Label = "Вт(10:00 Мск.)"
	case 7:
		point.Label = "Ср(10:00 Мск.)"
	case 8:
		point.Label = "Чт(10:00 Мск.)"
	case 9:
		point.Label = "Пт(10:00 Мск.)"
	case 10:
		point.Label = "Пт(18:00 Мск.)"
	}

	return point

}

func getSpAll(parseData [][]string, spIndex int) float64 {

	//fmt.Println("start get sp all")
	var allSpInTheFile float64
	for i := 1; i < len(parseData); i++ {
		//if parseData[i][5] != "Отложен" {
			allSpInTheFile += my_conv.ThisStrToFloat(parseData[i][spIndex])
			//allSpInTheFile += (my_conv.ThisStrToFloat(parseData[i][4]))/60/8
		//}
	}

	//fmt.Println("return allSpInTheFile=")
	//fmt.Println(allSpInTheFile)
	return allSpInTheFile

}

func getSpBurnt(parseData [][]string, spAll float64, statusIndex, spIndex int) float64 {

	var allSpBurntInTheFile float64

	for i := 1; i < len(parseData); i++ {

		//if parseData[i][5] == "Закрыт" || parseData[i][5] == "Закрыто" || parseData[i][5] == "Выполнено" || parseData[i][5]=="Отложен" {
		if parseData[i][statusIndex] == "Выполнено" || parseData[i][statusIndex] == "Закрыто" || parseData[i][statusIndex] == "Закрыт" {
			allSpBurntInTheFile += my_conv.ThisStrToFloat(parseData[i][spIndex])
			//allSpBurntInTheFile += (my_conv.ThisStrToFloat(parseData[i][4]))/60/8
		}

	}

	return spAll - allSpBurntInTheFile

}

func getSpPerfectBurnt(allSp float64, index int) float64 {

	switch index {
	case 0:
		return allSp
	case 1:
		return allSp * 0.9
	case 2:
		return allSp * 0.8
	case 3:
		return allSp * 0.7
	case 4:
		return allSp * 0.6
	case 5:
		return allSp * 0.5
	case 6:
		return allSp * 0.4
	case 7:
		return allSp * 0.3
	case 8:
		return allSp * 0.2
	case 9:
		return allSp * 0.1
	case 10:
		return allSp * 0
	}
	return allSp
}

func getEmptyBurnDownPoint(index int, allSp float64) BurnDownPoint {

	var point BurnDownPoint

	point.SpAll = allSp
	point.SpBurnt = -1
	point.SpPerfectBurnt = getSpPerfectBurnt(point.SpAll, index)

	switch index {
	case 0:
		point.Label = "Пн(10:00 Мск.)"
	case 1:
		point.Label = "Вт(10:00 Мск.)"
	case 2:
		point.Label = "Ср(10:00 Мск.)"
	case 3:
		point.Label = "Чт(10:00 Мск.)"
	case 4:
		point.Label = "Пт(10:00 Мск.)"
	case 5:
		point.Label = "Пн(10:00 Мск.)"
	case 6:
		point.Label = "Вт(10:00 Мск.)"
	case 7:
		point.Label = "Ср(10:00 Мск.)"
	case 8:
		point.Label = "Чт(10:00 Мск.)"
	case 9:
		point.Label = "Пт(10:00 Мск.)"
	case 10:
		point.Label = "Пт(18:00 Мск.)"
	}

	return point
}
