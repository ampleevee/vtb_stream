package my_scrum

import (
	"vtb_jira/dir_functions"
	"vtb_jira/my_conv"
	"vtb_jira/my_csv"
	"fmt"
)

func PrintAllSpByStatusesForChart(dir string) {

	// нам нужно сначала получить один статус в хронологическом порядке и сохранить его
	// затем мы получаем следующи статус той же функцией, но результат корректируем с учетом суммы с предыдущим статусом

	// переменная, для хранения хронологии сп по исследуемому статусу
	var status [][]Status

	//status = append(status, GetDynamicsStatusesForChart(Status{"Завершено", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Тестирование", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Готово для тестирования", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"В работе", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Готово для разработки", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Черновик", 0, 0}, dir))

	status = append(status, GetDynamicsStatusesForChart(Status{"Закрыто", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Закрыт", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Выполнено", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Подтверждение", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Приостановлено", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"В работе", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Создано", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Отложен", 0, 0}, dir))

	for i := 1; i < len(status); i++ {
		for j := 0; j < len(status[i]); j++ {
			status[i][j].sp = status[i-1][j].sp + status[i][j].sp
		}
	}
	//for i := 0; i < len(status); i++ {
	//	for j := 0; j < len(status[i]); j++ {
	//		fmt.Println(status[i][j].status, status[i][j].sp)
	//	}
	//}
	for i := 0; i < len(status); i++ {
		fmt.Printf("{\n")
		fmt.Printf("label: '%v',\n", status[i][0].status)

		switch status[i][0].status {
		case "Отложен":
			fmt.Printf("backgroundColor: 'rgb(158,158,158)',\n")
		case "Создано":
			fmt.Printf("backgroundColor: 'rgb(139,0,255)',\n")
		case "В работе":
			fmt.Printf("backgroundColor: 'rgb(95,137,244)',\n")
		case "Приостановлено":
			fmt.Printf("backgroundColor: 'rgb(95,192,244)',\n")
		case "Подтверждение":
			fmt.Printf("backgroundColor: 'rgb(105,244,95)',\n")
		case "Выполнено":
			fmt.Printf("backgroundColor: 'rgb(244,237,95)',\n")
		case "Закрыт":
			fmt.Printf("backgroundColor: 'rgb(244,177,95)',\n")
		case "Закрыто":
			fmt.Printf("backgroundColor: 'rgb(244,95,95)',\n")
		}

		fmt.Printf(" data: [")

		for j := 0; j < len(status[i]); j++ {
			if j < len(status[i])-1 {
				fmt.Printf(" %v,", status[i][j].sp)
				continue
			}
			fmt.Printf(" %v", status[i][j].sp)
		}
		fmt.Printf("]")
		fmt.Printf("\n},")
		fmt.Printf("\n")

	}

}

func GetDynamicsStatusesForChart(status Status, dir string) []Status {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)
	var res []Status
	for i := 0; i < len(allCsvFiles); i++ {

		res = append(res, GetValuesInTheFileForChart(status, allCsvFiles[i], dir))

	}

	return res

}

func GetValuesInTheFileForChart(status Status, file string, dir string, statusIndex, spIndex int) Status {

	data := my_csv.CsvParse(file, dir)

	var allSp float64

	allSp = 0

	for i := 1; i < len(data); i++ {

		allSp += my_conv.ThisStrToFloat(data[i][spIndex])

		if data[i][statusIndex] == status.status {

			status.sp += my_conv.ThisStrToFloat(data[i][spIndex])

		}

	}

	status.percentage = int((status.sp * 100 / allSp) + 0.5)
	return status

}
