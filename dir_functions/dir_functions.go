package dir_functions

import (
	"io/ioutil"
	"log"
)

func SearchAllCsvFiles(dirPath string) []string {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Fatal(err)
	}

	var allCsvFiles []string

	for _, file := range files {
		fileName := file.Name()

		if fileName[len(fileName)-4:] == ".csv" {
			allCsvFiles = append(allCsvFiles, fileName)
		}

		//fmt.Println(fileName[len(fileName)-4:], file.IsDir())
	}

	return allCsvFiles

}
